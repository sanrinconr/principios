# Principios de diseño

- Josue Alexander Nuñez Prada - 20172020071
- Santiago Rincon Robelto - 20172020084

Principios utilizados 

1. Principio del menor  (Demeter)
    * Una moto nunca se relacionara un con objeto del tipo coche
2. Principio DRY (Don't Repeat Yourself)
    * Se utiliza la herencia para asi evitar la repeticion de codigo en cuanto a la obtencion y asignacion de color, tipo de silla, si tiene o no aire aconficionado y el tipo 
3. Responsabilidad unica (SOLID)
    * Este principio se evidencia durante todo el programa, sin embargo se puede observar principalmente en la clase Vendedor y ActualizaVendedor para si evitar que Vendedor se encarge de venderCoche y a la vez cambiarSede (violando asi el principio)
4. Principio KISS (Keep It Simple, Stupid!)
    * A lo largo del programa se observa la simplicidad de todas las funciones, las salidas del programa son siempre identicas (en notacion) y el formato es dado unicamente por una instruccion (print(s) de launcher.py)
5. Principio abierto/cerrado (SOLID)
    * Realizar la extension de un nuevo coche es muy facil, tal cual lo establece este principio, caso contrario seria que por ejemplo se tuviese que determinar el valor promedio de un coche a partir de su marca con un condicional en la clase launcher
 
