from Coche import Coche
from Camioneta import Camioneta
from Deportivo import Deportivo
from Vendedor import Vendedor
from ActualizarVendedor import ActualizarVendedor

deportivo1 = Deportivo("Audi","Plateado","Cuero",True,"Carreras")
vendedor1= Vendedor("Carlos","Quevedo","Bogota")

actualizador = ActualizarVendedor()
actualizador.cambiarSede(vendedor1,"Medellin")

print(vendedor1.venderCoche(deportivo1)+"\n")
print("DATOS DEL VENDEDOR:")
print(vendedor1.getNombre() + " " +vendedor1.getApellido())
print("Sede: " + vendedor1.getSede())
